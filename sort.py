#Script used to analyze logs from attack server.
#Very quickly developped, mostly as it had to be only slightly specialized
import re
from datetime import datetime
attackpage="http://cpsc525w2018.ddns.net/ScriptlessAttacks/scriptless_attackc/"
nocache_attackpage="http://cpsc525w2018.ddns.net/ScriptlessAttacks/scriptless_attack/"
attackpage="http://cpsc525w2018.ddns.net/ScriptlessAttacks/scriptless_attackc2/"
nocache_attackpage="http://cpsc525w2018.ddns.net/ScriptlessAttacks/scriptless_attack2/"

##LOG CONSTANTS
#Attack Types
#scriptless_attack2 - NON SSL - 0
#scriptless_attackc2 - NON SSL CACHED - 1
#scriptless_attack - SSL -2
#scriptless_attackc - SSL CACHED -3
NON_SSL =0
NON_SSL_CACHED = 1
SSL = 2
SSL_CACHED = 3
#USERAGENT TYPES
#Deterfox (firefox 51)
#Firefox (firefox 59)
#Chrome
#Edge
#IE
UA_DETER=0
UA_FIREFOX=1
UA_CHROME=2
UA_EDGE=3
UA_IE = 4
#RESULTTYPES
RE_HIT = 0
RE_MISS = 1

def matchAttack(a1,a2):
    #print("Comparing: " + a1+ "\n" + a2)
    #input()
    if "scriptless_attackc2" in a1 and  "scriptless_attackc2" in a2:
        return True
    elif "scriptless_attack2" in a1 and "scriptless_attack2" in a2:
        return True
    elif "scriptless_attackc" in a1 and "scriptless_attackc" in a2:
        return True
    elif "scriptless_attack" in a1 and "scriptless_attack" in a2:
        return True
    return False

def complementedQuery(a1,a2):
    if ("baseline" in a1[2] and "attack" in a2[2]):
        return True
    elif ("baseline" in a2[2] and "attack" in a1[2]):
        return True
    else:
        return False

def store_result(a1,a2):
    global attack_results
    UA = 0
    RE = 0
    AT = 0
    #Get attack type
    if "scriptless_attackc2" in a1[5]:
        AT=1
    elif "scriptless_attack2" in a1[5]:
        AT=0
    elif "scriptless_attackc" in a1[5]:
        AT=3
    elif "scriptless_attack" in a1[5]:
        AT=2

    #Determine useragent
    #Contains 
    #Edge? (Needs to come before chrome, as it has chrome in its UA)`
    #Chrome and not Edge
    #Firefox/51.0 - Specific Deterfox version
    #Firefox - Any other stable release on linux
    #Trident - IE
    if "Edge" in a1[6]:
        UA = UA_EDGE
    elif "Chrome" in a1[6]:
        UA = UA_CHROME
    elif "Firefox/51.0" in a1[6]:
        UA = UA_DETER
    elif "Firefox" in a1[6]:
        UA = UA_FIREFOX
    elif "Trident" in a1[6]:
        UA = UA_IE

    #Determine result type
    if ("baseline" in a1[2]):
        if a1[1] > a2[1] :
            RE = RE_MISS
        elif a1[1] < a2[1]:
            RE = RE_HIT
    elif ("baseline" in a2[2]):
        if a1[1] < a2[1] :
            RE = RE_MISS
        elif a1[1] > a2[1]:
            RE = RE_HIT
            
    #Finally, store result
    attack_results[AT][UA][RE] =    attack_results[AT][UA][RE]+1

def print_attacktyperesults(at):
#UA_DETER=0
#UA_FIREFOX=1
#UA_CHROME=2
#UA_EDGE=3
#UA_IE = 4
    global attack_results
    print("DETERFOX: " + "\tCACHE HITS: " + str(attack_results[at][UA_DETER][RE_HIT]) + "\tCACHE MISSES: " +  str(attack_results[at][UA_DETER][RE_MISS]))
    print("FIREFOX: " + "\tCACHE HITS: " + str(attack_results[at][UA_FIREFOX][RE_HIT]) + "\tCACHE MISSES: " + str(attack_results[at][UA_FIREFOX][RE_MISS]))
    print("CHROME: " + "\tCACHE HITS: " + str(attack_results[at][UA_CHROME][RE_HIT]) + "\tCACHE MISSES: " +  str(attack_results[at][UA_CHROME][RE_MISS]))
    print("EDGE: " + "\t\tCACHE HITS: " + str(attack_results[at][UA_EDGE][RE_HIT]) + "\tCACHE MISSES: " +  str(attack_results[at][UA_EDGE][RE_MISS]))
    print("IE: " + "\t\tCACHE HITS: " + str(attack_results[at][UA_IE][RE_HIT]) + "\tCACHE MISSES: " +  str(attack_results[at][UA_IE][RE_MISS]))

def print_results():
    global attack_results
    print("ATTACK TYPE - NON SSL")
    print_attacktyperesults(0)
    print("ATTACK TYPE - NON SSL CACHED")
    print_attacktyperesults(1)
    print("ATTACK TYPE - SSL")
    print_attacktyperesults(2)
    print("ATTACK TYPE - SSL CACHED")
    print_attacktyperesults(3)

regex='([(\d\.)]+) - - (.*?) "(.*?)" (\d+) (\d+) "(.*?)" "(.*?)"'
#Read in log files
logfile="apache2/access.log"
f = open(logfile).read()
logfile = "apache2/access.log.1"
f += open(logfile).read()
logfile = "apache2/access.log.2"
f += open(logfile).read()
logfile = "lastresults/access.log"
f += open(logfile).read()
logfile = "lastresults/access.log.1"
f += open(logfile).read()
logfile = "lastresults/access.log.2"
f += open(logfile).read()
#Filter out everything that isn't an html document
logs=[]
for i in f.split("\n"):
    if ".html" in i:
       l = list(re.match(regex,i).groups()) #Apply regex to grab relevent fields
       l[1] = datetime.strptime(l[1], "%d/%b/%Y:%H:%M:%S-%f") # Convert time into datetime object for easy comparison
       logs += [l]

#LOG FORMAT [{IP}, {TIME}, {QUERY}, {ID},{ID}, {REFERER}, {USERAGENT}]
global attack_results   
#[Attack Type, UserAgent, CacheHit/Miss 0/1]
#attack_results = [["SSL_CACHED",0,0],["SSL_UNCACHED",0,0],["CACHED",0,0],["UNCACHED",0,0]]

attack_results =  [[[0 for col in range(2)]for row in range(5)] for x in range(4)]
#attack_results[AttackType][UserAgent][CacheHit/CacheMiss]
#4 attack types
#5 different user agents
#2 different types of results

while True:
#POP a log off    
    elem = logs.pop(0)
    for i in range(0,len(logs)):
        if (elem[0] == logs[i][0]): #Do the IPs match?
            if (elem[6] == logs[i][6]): #Do Useragents match?
                if (matchAttack(elem[5],logs[i][5])): #Do the attacks/refers match?
                    if (complementedQuery(elem,logs[i])): #Is the query complemented?
                        timedif = elem[1] - logs[i][1]
                        if (timedif.total_seconds() < 5): #If theres been more than 5 seconds between queries
                            store_result(elem, logs[i])
                            del logs[i]
                            break
    if (len(logs) < 2): #If there's less than 2, there's no way a match can be made.
        break

#print(attack_results)
print_results()
